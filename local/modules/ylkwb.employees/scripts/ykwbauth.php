<?

class __YLWB2Auth
{
    const EXTERNAL_AUTH_ID = 'YLWB';
    const EXTERNAL_AUTH_NAME = "Ylkwb auth table";
    const EXTERNAL_AUTH_PARAM = 'YLWB_USER_ID';
    const EXTERNAL_AUTH_DB = 'ylkwb_employees_users';
    const EXTERNAL_AUTH_GLOBAL = 'YLWB_GLOBAL_PARAM';

    function OnUserLoginExternal(&$arArgs)
    {    
        extract($arArgs);

        global $USER;
        
        if (\Bitrix\Main\Loader::includeModule('ylkwb.employees'))
        {
            $query = new Ylkwb\Employees\Core\ManagerDb();
            
            $result = $query
                ->table(self::EXTERNAL_AUTH_DB)
                ->where('LOGIN', $arArgs['LOGIN'])
//                ->isPrint()  
                ->select(); 
            
            foreach ($result as $userExternal)
            {
                $isVerify = password_verify($arArgs['PASSWORD'], $userExternal['PASSWORD']);

                if ($isVerify)
                {                    
                    $arFields = Array(
                        "LOGIN" => $userExternal['LOGIN'] . '_' . self::EXTERNAL_AUTH_ID,
                        "NAME" => $userExternal['LOGIN'],
                        "PASSWORD" => $arArgs['PASSWORD'],
                        "EMAIL" => 'noemail@example.com',
                        "ACTIVE" => "Y",
                        "EXTERNAL_AUTH_ID"=> self::EXTERNAL_AUTH_ID,
                        "LID" => SITE_ID
                    );
                    
                    $oUser = new CUser;
                    
                    $res = CUser::GetList(
                            $order,
                            $by, 
                            Array(
                                "LOGIN_EQUAL_EXACT"=>$userExternal['LOGIN']. '_' . self::EXTERNAL_AUTH_ID, 
                                "EXTERNAL_AUTH_ID"=>self::EXTERNAL_AUTH_ID
                            )
                        );
                    
                    if(!($ar_res = $res->Fetch()))
                    {
                        $ID = $oUser->Add($arFields);
                        
                        $arUserGroups = CUser::GetUserGroup($ID);
                            
                        //bitrix24 
                        $arUserGroups[] = 12;
                        
                        CUser::SetUserGroup($ID, $arUserGroups);
                    }
                    else
                    {
                        $ID = $ar_res["ID"];
                        $oUser->Update($ID, $arFields);
                    }

                    if( $ID > 0 )
                    {
                        $arArgs["store_password"] = "N";
                        
                        $GLOBALS[self::EXTERNAL_AUTH_GLOBAL] = 'Y';
                        
                        $update = $query
                            ->table(self::EXTERNAL_AUTH_DB)
                            ->where('ID', $userExternal['ID'])
                            ->set([
                                'LAST_AUTH' => (new \DateTime("now", new \DateTimeZone('Etc/GMT-3')))->format(\DateTime::ISO8601)
                            ]) 
//                            ->isPrint()
                            ->update();
                        
                        return $ID;
                    }
                }
            }
        }
    }

    function OnExternalAuthList()
    {
        return Array(
            Array("ID"=>self::EXTERNAL_AUTH_ID, "NAME"=>self::EXTERNAL_AUTH_NAME)
        );
    }
    
    function onBeforeUserLoginByHttpAuth (&$arAuth)
    {
        global $USER, $APPLICATION;
        
        $context = \Bitrix\Main\Context::getCurrent();

        $request = $context->getRequest();
        
        if ( !$USER->IsAuthorized() ) 
        {
            $login = $request->get("login");
            $passwd = $request->get("passwd");
            
            if (is_string($login) && strlen($login) > 0
                    && is_string($passwd) && strlen($passwd) > 0)
            {
                $login = $request->get("login");
                $passwd = $request->get("passwd");

                $arAuthResult = $USER->Login($request->get("login"), $request->get("passwd"));
                
                $APPLICATION->arAuthResult = $arAuthResult;
            }
        }
        
        return false;
    }

    function OnEpilog ()
    {
        global $USER, $APPLICATION;
        
        if (isset($GLOBALS[self::EXTERNAL_AUTH_GLOBAL])
                &&
                $GLOBALS[self::EXTERNAL_AUTH_GLOBAL] === 'Y')
        {
            $USER->Logout();
        }
    }
}
?>