<?php

namespace Ylkwb\Employees\Interfaces;

interface iEmployeesTableSearch
{
    public function search(string $searchValue) : array;
    public function index(array $items): bool;
    public function nameTableSearch();
}
