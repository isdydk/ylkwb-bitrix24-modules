<?php

namespace Ylkwb\Employees\Interfaces;

interface iEmployeesTable
{
    public function add(array $fields): int;

    public function update(int $id, array $fields): bool;

    public function delete(int $id): bool;

    public function get(int $id): array;

    public function getList(
            array $order = null,
            array $filter = null,
            array $select = null
    ): array;

    public function nameTable();
}
