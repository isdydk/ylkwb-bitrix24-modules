<?php

namespace Ylkwb\Employees\Table;

use Ylkwb\Employees\Interfaces\iEmployeesTable;
use Ylkwb\Employees\Core\ManagerDb;
use Ylkwb\Employees\Validate;

class EmployeesPost implements iEmployeesTable
{
    private $mdb;
    private $valid;

    public function __construct()
    {
        $this->mdb = new ManagerDb();

        $this->valid = new Validate();
    }
    
    public function nameTable()
    {
        return 'ylkwb_employees_post';
    }
    
    /**
     * Получить записи
     * 
     * @param array $ids
     * @return array
     */
    public function getList(
            array $order = null, 
            array $filter = null, 
            array $select = null 
            ) : array
    {
        $query = $this->mdb
                    ->table($this->nameTable());
        
        if ($this->valid->isValidArray($select))
        {
            $query = $query->columns($select);
        }
        else
        {
            $query = $query->columns(['ID', 'NAME']);
        }
         
        if ($this->valid->isValidArray($order))
        {
            foreach ($order as $key=>$sort)
            {
                $query = $query->order($key, $sort);
            }
        }
        else
        {
            $query = $query->order('ID', 'DESC');
        }
        
        if ($this->valid->isValidArray($filter))
        {
            foreach ($filter as $key=>$value)
            {
                if ($key == 'ID')
                {
                    $mapId = array_map(function ($id) {
                        return $this->valid->getInt($id);
                    }, $value);

                    $filterId = array_filter($mapId, function ($id) {
                        return $this->valid->isValidInt(intval($id));
                    });

                    $iqueId = array_unique($filterId);
                    
                    if ($this->valid->isValidArray($iqueId))
                    {
                        $query = $query->whereIn('ID', $iqueId);
                    }
                }
            }
        }
        
        return $query->select();
    }
    
    /**
     * Получить запись
     * 
     * @param int $id
     * @return array
     */
    public function get(int $id) : array
    {
        if ( $this->valid->isValidInt($id) )
        {
            $this->mdb = new ManagerDb();
            
            return $this->mdb
                ->table($this->nameTable())
                ->columns(['ID', 'NAME'])
                ->where('ID', $id)
                ->order('ID', 'DESC')
                ->select();
        }
        
        return [];
    }
    
    /**
     * Добавить запись
     * 
     * @param array $fields
     * @return int
     */
    public function add(array $fields) : int
    {
        $data = [];
        
        foreach ($fields as $key=>$field)
        {
            $value = $this->valid->getMixed($field);
            
            if ( $this->valid->isValidMixed($field) )
            {
                $data[$key] = $value;
            }
        }

        if ( $this->valid->isValidArray($data) )
        {
            $this->mdb = new ManagerDb();
            
            $keys = array_keys($data);
            
            return $this->mdb
                ->table($this->nameTable())
                ->insertPrimaryKey('ID')
                ->insertKeys($keys)
                ->insertValue($data)
                ->insert();
        }
        
        return 0;
    }
    
    /**
     * Обновить запись
     * 
     * @param int $id
     * @param array $fields
     * @return bool
     */
    public function update(int $id, array $fields) : bool
    {
        $data = [];
        
        foreach ($fields as $key=>$field)
        {
            $value = $this->valid->getMixed($field);
            
            if ( $this->valid->isValidMixed($field) )
            {
                $data[$key] = $value;
            }
        }
        
        if (
                $this->valid->isValidInt($id)
                && $this->valid->isValidArray($data)
            )
        {
            $this->mdb = new ManagerDb();
            
            return $this->mdb
                ->table($this->nameTable())
                ->set($data)
                ->where('ID', $id)
                ->update();
        }
        
        return false;
    }
    
    /**
     * Удалить запись
     * 
     * @param int $id
     * @return bool
     */
    public function delete(int $id) : bool
    {
        if ( $this->valid->isValidInt($id) )
        {
            $this->mdb = new ManagerDb();
            
            return $this->mdb
                ->table($this->nameTable())
                ->where('ID', $id)
                ->delete();
        }
        
        return false;
    }
    
    /**
     * Получить массив полей
     * 
     * @param string $name
     * @return array
     */
    public function getFields(string $name = null) : array
    {
        $data = [];
        
        $value = $this->valid->getMixed($name);

        if ( $this->valid->isValidMixed($name) )
        {
            $data['NAME'] = $value;
        }
        
        return $data;
    }
}
