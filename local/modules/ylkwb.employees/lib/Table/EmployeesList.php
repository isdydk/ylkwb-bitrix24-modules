<?php

namespace Ylkwb\Employees\Table;

use Ylkwb\Employees\Interfaces\iEmployeesTable;
use Ylkwb\Employees\Interfaces\iEmployeesTableSearch;
use Ylkwb\Employees\Core\ManagerDb;
use Ylkwb\Employees\Validate;

class EmployeesList implements iEmployeesTable, iEmployeesTableSearch
{
    private $mdb;
    private $valid;

    public function __construct()
    {
        $this->mdb = new ManagerDb();

        $this->valid = new Validate();
    }
    
    public function nameTable()
    {
        return 'ylkwb_employees_list';
    }
    
    public function nameTableSearch()
    {
        return 'ylkwb_employees_list_index';
    }
    
    /**
     * Получить записи
     * 
     * @param array $ids
     * @return array
     */
    public function getList(
            array $order = null, 
            array $filter = null, 
            array $select = null 
            ) : array
    {
        $query = $this->mdb
            ->table($this->nameTable());
        
        if ($this->valid->isValidArray($select))
        {
            foreach ($select as $key)
            {
                if ($key == 'OFFICE')
                {
                    $query = $query
                            ->columnJoin('ylkwb_employees_office', 'NAME', 'OFFICE');
                }
                elseif ($key == 'ADDRESS')
                {
                    $query = $query->columnJoin('ylkwb_employees_office', 'ADDRESS');
                }
                elseif ($key == 'POST')
                {
                    $query = $query
                            ->columnJoin('ylkwb_employees_post', 'NAME', 'POST');
                }
                else
                {
                    $query = $query->column($key);
                }
            }
            
            if (in_array('OFFICE', $select) || in_array('ADDRESS', $select))
            {
                $query = $query->leftJoin('ylkwb_employees_office', 'OFFICE_ID');
            }
            
            if (in_array('POST', $select))
            {
                $query = $query->leftJoin('ylkwb_employees_post', 'POST_ID');
            }
                
        }
        else
        {
            $query = $query->columns(['ID', 'FIO', 'PHONE'])
            ->columnsJoin([
                ['ylkwb_employees_office', 'NAME', 'OFFICE'],
                ['ylkwb_employees_office', 'ADDRESS'],
                ['ylkwb_employees_post', 'NAME', 'POST'],
            ])
            ->leftJoin('ylkwb_employees_office', 'OFFICE_ID')
            ->leftJoin('ylkwb_employees_post', 'POST_ID');
        }
        
        if ($this->valid->isValidArray($order))
        {
            foreach ($order as $key=>$sort)
            {
                $query = $query->order($key, $sort);
            }
        }
        else
        {
            $query = $query->order('ID', 'DESC');
        }
        
        if ($this->valid->isValidArray($filter))
        {
            foreach ($filter as $key=>$value)
            {
                if ($key == 'ID')
                {
                    $mapId = array_map(function ($id) {
                        return $this->valid->getInt($id);
                    }, $value);

                    $filterId = array_filter($mapId, function ($id) {
                        return $this->valid->isValidInt(intval($id));
                    });

                    $iqueId = array_unique($filterId);
                    
                    if ($this->valid->isValidArray($iqueId))
                    {
                        $query = $query->whereIn('ID', $iqueId);
                    }
                }
            }
        }
        
        return $query
            ->select();
    }
    
    /**
     * Получить запись
     * 
     * @param int $id
     * @return array
     */
    public function get(int $id) : array
    {
        if ( $this->valid->isValidInt($id) )
        {
            return $this->mdb
                ->table($this->nameTable())
                ->columns(['ID', 'FIO', 'PHONE'])
                ->columnsJoin([
                    ['ylkwb_employees_office', 'NAME', 'OFFICE'],
                    ['ylkwb_employees_office', 'ADDRESS'],
                    ['ylkwb_employees_post', 'NAME', 'POST'],
                ])
                ->leftJoin('ylkwb_employees_office', 'OFFICE_ID')
                ->leftJoin('ylkwb_employees_post', 'POST_ID')
                ->where('ID', $id)
                ->order('ID', 'DESC')
                ->select();
        }
        
        return [];
    }
    
    /**
     * Добавить запись
     * 
     * @param array $fields
     * @return int
     */
    
    public function add(array $fields) : int
    {
        $data = [];
        
        foreach ($fields as $key=>$field)
        {
            $value = $this->valid->getMixed($field);
            
            if ( $this->valid->isValidMixed($field) )
            {
                $data[$key] = $value;
            }
        }
        
        if ( $this->valid->isValidArray($data) )
        {
            $this->mdb = new ManagerDb();
            
            $keys = array_keys($data);
            
            return $this->mdb
                ->table($this->nameTable())
                ->insertPrimaryKey('ID')
                ->insertKeys($keys)
                ->insertValue($data)
                ->insert();
        }
        
        return 0;
    }
    
    /**
     * Обновить запись
     * 
     * @param int $id
     * @param array $fields
     * @return bool
     */
    public function update(int $id, array $fields) : bool
    {
        $data = [];
        
        foreach ($fields as $key=>$field)
        {
            $value = $this->valid->getMixed($field);
            
            if ( $this->valid->isValidMixed($field) )
            {
                $data[$key] = $value;
            }
        }
        
        if (
                $this->valid->isValidInt($id)
                && $this->valid->isValidArray($data)
            )
        {
            $this->mdb = new ManagerDb();
            
            return $this->mdb
                ->table($this->nameTable())
                ->set($data)
                ->where('ID', $id)
                ->update();
        }
        
        return false;
    }
    
    /**
     * Удалить запись
     * 
     * @param int $id
     * @return bool
     */
    public function deleteIndex(int $id) : bool
    {
        if ( $this->valid->isValidInt($id) )
        {
            $this->mdb = new ManagerDb();
            
            return $this->mdb
                ->table($this->nameTableSearch())
                ->where('ID', $id)
                ->delete();
        }
        
        return false;
    }
    
    /**
     * Удалить запись
     * 
     * @param int $id
     * @return bool
     */
    public function delete(int $id) : bool
    {
        if ( $this->valid->isValidInt($id) )
        {
            $this->mdb = new ManagerDb();
            
            $isIndex = $this->deleteIndex($id);
            
            if ($isIndex)
            {
                return $this->mdb
                    ->table($this->nameTable())
                    ->where('ID', $id)
                    ->delete();
            }
        }
        
        return false;
    }
    
    /**
     * Поиск записи
     * 
     * @param string $searchValue
     * @return array
     */
    public function search(string $searchValue) : array
    {
        if (is_string($searchValue) && strlen($searchValue) > 0)
        {
            return $this->mdb
                ->table($this->nameTable())
                ->columns(['ID', 'FIO', 'PHONE'])
                ->columnsJoin([
                    ['ylkwb_employees_office', 'NAME', 'OFFICE'],
                    ['ylkwb_employees_office', 'ADDRESS'],
                    ['ylkwb_employees_post', 'NAME', 'POST'],
                ])
                ->leftJoin('ylkwb_employees_office', 'OFFICE_ID')
                ->leftJoin('ylkwb_employees_post', 'POST_ID')
                ->search($this->nameTableSearch(), 'ID', "+*$searchValue*")
                ->order('ID', 'DESC')
                ->select();
        }
        
        return []; 
    }
    
    /**
     * Создание/обновления индекса
     * 
     * @param string $items
     * @return bool
     */
    public function index(array $items): bool 
    {
        if (is_array($items) && count($items) > 0)
        {
            $result = $this->mdb
                ->table($this->nameTableSearch())
                ->columns(['ID'])
                ->whereIn('ID', array_keys($items))
                ->select();
            
            $arIndexes = [];
            
            foreach ($result as $row)
            {
                if (intval($row['ID']) > 0)
                {
                    $arIndexes[] = intval($row['ID']);
                }
            }
            
            foreach ($items as $id=>$searchContent)
            {
                if (in_array($id, $arIndexes))
                {
                    $res = $this->mdb
                        ->table($this->nameTableSearch())
                        ->set([
                            'SEARCH_CONTENT' => $searchContent
                        ])
                        ->where('ID', $id)
                        ->update();
                }
                else 
                {
                    $res = $this->mdb
                        ->table($this->nameTableSearch())
                        ->insertKeys([
                            'ID', 'SEARCH_CONTENT'
                        ])
                        ->insertValue([
                            'ID' => $id,
                            'SEARCH_CONTENT' => $searchContent,
                        ])
                        ->insert();
                }
            }
            
            return true;
        }
        
        return false; 
   }
   
    /**
     * Получить массив полей
     * 
     * @param string $fio
     * @param int $phone
     * @param int $post
     * @param int $office
     * @return array
     */
    public function getFields(string $fio = null, int $phone = null, int $post = null, int $office = null) : array
    {
        $data = [];
        
        $valueFio = $this->valid->getString($fio);

        if ( $this->valid->isValidString($fio) )
        {
            $data["FIO"] = $valueFio;
        }
        
        $valuePhone = $this->valid->getInt($phone);

        if ( $this->valid->isValidInt($valuePhone) )
        {
            $data["PHONE"] = $valuePhone;
        }
        
        $valuePost = $this->valid->getInt($post);

        if ( $this->valid->isValidInt($valuePost) )
        {
            $data["POST_ID"] = $valuePost;
        }
        
        $valueOffice = $this->valid->getInt($office);

        if ( $this->valid->isValidInt($valueOffice) )
        {
            $data["OFFICE_ID"] = $valueOffice;
        }
        
        return $data;
    }
}
