<?php

namespace Ylkwb\Employees\Search;

use Ylkwb\Employees\Table\EmployeesList;

class EmployeesSearchContentBuilder
{
    public function bulkBuild($itemsId) 
    {
        if (is_array($itemsId) && count($itemsId) > 0)
        {
            $el = new EmployeesList();

            $arItems = $el->getList([], [
                'ID' => $itemsId
            ]);

            if (is_array($arItems) && count($arItems))
            {
                foreach ($arItems as $item)
                {
                    $searchContent = implode(' ', $item);

                    $ids[$item['ID']] = $searchContent;
                }
                
                $el->index($ids);
            }
        }
    }
}
