<?php
namespace Ylkwb\Employees\Agent\Search;

use Ylkwb\Employees\Orm\EmployeesTable;
use Ylkwb\Employees\Search\EmployeesSearchContentBuilder;

class EmployeesSearchContentRebuildAgent extends SearchContentRebuildAgent
{   
    
    public function getOptionNames() : array 
    {
        return [
            'SEARCH_CONTENT' => '~YLKWB_REBUILD_EMPLOYEES_SEARCH_CONTENT',
            'SEARCH_CONTENT_PROGRESS' => '~YLKWB_REBUILD_EMPLOYEES_SEARCH_CONTENT_PROGRESS'
        ];
    }
    
    public function getTotalCount() : int
    {
        $resDb = EmployeesTable::getCount();
        
        return $resDb;
    }
    
    public function prepareItemIDs(int $offsetID = 0, int $limit = 50) : array
    {
        $result = [];
        
        $resDb = EmployeesTable::getList([
            'select' => [
                'ID'
            ],
            'filter' => [
                '>ID' => $offsetID
            ],
            'order' => [
                'ID' => 'ASC'
            ],
            'limit' => $limit,
        ]);
        
        while ($itemFields = $resDb->Fetch())
        {
            $result[] = $itemFields['ID'];
        }
        
        
        return $result;
    }
    
    public function rebuild(array $itemIDs)
    {
        $builder = new EmployeesSearchContentBuilder();
        $builder->bulkBuild($itemIDs);
    }
    
    public static function getInstance()
    {
        if(self::$instance === null)
        {
            self::$instance = new EmployeesSearchContentRebuildAgent();
        }
        
        return self::$instance;
    }
}

