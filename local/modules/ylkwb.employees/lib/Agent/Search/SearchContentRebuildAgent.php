<?php
namespace Ylkwb\Employees\Agent\Search;

use \Bitrix\Main\Config\Option;

abstract class SearchContentRebuildAgent 
{   
    protected static $instance;

    const ITEM_LIMIT = 2;
    
    abstract public function getOptionNames() : array;
    abstract public function getTotalCount() : int;
    abstract public function prepareItemIDs(int $offsetID = 0, int $limit = 50) : array;
    abstract public function rebuild(array $itemIDs);
    abstract public static function getInstance();
    
    public static function run()
    {
        return static::doRun() ? get_called_class().'::run();' : '';
    }
    
    public static function doRun()
    {
        $instance = static::getInstance();
        
        $progressData = $instance->getProgressData();
        
        $offsetID = isset($progressData['LAST_ITEM_ID']) ? (int)($progressData['LAST_ITEM_ID']) : 0;
        
        if ($offsetID >= $instance->getTotalCount())
        {
            $offsetID = 0;
        }
        
        $itemIDs = $instance->prepareItemIDs($offsetID, self::ITEM_LIMIT);
        
        if(is_array($itemIDs) && count($itemIDs) > 0)
        {
            $instance->rebuild($itemIDs);

            $progressData['LAST_ITEM_ID'] = intval(end($itemIDs));

            $instance->setProgressData($progressData);
        }
        
        return true;
    }
      
    protected function getProgressData()
    {
        $ops = $this->getOptionNames();
        
        $s = Option::get('ylkwb.employees', $ops['SEARCH_CONTENT_PROGRESS']);

        $data = $s !== '' ? unserialize($s, ['allowed_classes' => false]) : null;

        if(!is_array($data))
        {
            $data = array();
        }

        $data['LAST_ITEM_ID'] = isset($data['LAST_ITEM_ID']) ? (int)($data['LAST_ITEM_ID']) : 0;

        return $data;
    }
    
    protected function setProgressData(array $data)
    {
        $ops = $this->getOptionNames();
        
        Option::set('ylkwb.employees', $ops['SEARCH_CONTENT_PROGRESS'], serialize($data));
    }
}

