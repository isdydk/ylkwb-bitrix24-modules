<?php
namespace Ylkwb\Employees;

class Validate 
{
    /**
     * Получить валидное занчение
     * 
     * @param string $reg
     * @param type $val
     * @param type $default
     * @return string
     */
    public function getValue(string $reg, $val = null, $default = '') : string
    {
        return !is_null($val)
            ? preg_replace($reg, "", trim($val))
            : $default;
    }
    
    /**
     * Получить валидную строку
     * 
     * @param type $val
     * @param type $default
     * @return string
     */
    public function getString($val = null, $default = '') : string
    {
        return $this->getValue("/[^A-Za-zА-Яа-я ]/iu", $val, $default);
    }
    
    /**
     * Получить валидное число с плавающей точкой
     * @param type $val
     * @param type $default
     * @return float
     */
    public function getFloat($val = null, $default = 0) : float
    {
        return floatval($this->getValue("/[^0-9\.]/", $val, $default));
    }
    
    /**
     * Получить валидное число
     * 
     * @param type $val
     * @param type $default
     * @return int
     */
    public function getInt($val = null, $default = 0) : int
    {
        return intval($this->getValue("/[^0-9]/", $val, $default));
    }
    
    /**
     * Получить валидное смешенное занчение
     * 
     * @param type $val
     * @param type $default
     * @return string
     */
    public function getMixed($val = null, $default = 0) : string
    {
        return $this->getValue("/[^A-Za-zА-Яа-я0-9-,\.\*\(\) ]/iu", $val, $default);
    }
    
    /**
     * Если валидная строка
     * 
     * @param type $val
     * @return bool
     */
    public function isValidString($val) : bool
    {
        return is_string($val) && !empty($val);
    }
    
    /**
     * Если валидное число
     * 
     * @param type $val
     * @return bool
     */
    public function isValidInt($val) : bool
    {
        return is_int($val) && intval($val) > 0;
    }
    
    /**
     * Если валидный массив
     * 
     * @param type $val
     * @return bool
     */
    public function isValidArray($val) : bool
    {
        return is_array($val) && count($val) > 0;
    }
    
    /**
     * Если валидное смешенное значение
     * 
     * @param type $val
     * @return bool
     */
    public function isValidMixed($val) : bool
    {
        return !empty($val) && !!$val;
    }
    
    /**
     * Получить телефон
     * 
     * @param type $val
     * @return int
     */
    public function getFormatedPhone($val = null) : int
    {
        $value = $this->getInt($val);
        
        //1000-9999
        if ($value > 999 && $value < 10000)
        {
            return $value;
        }
        
        return 0;
    }
}
