<?php
namespace Sntinvest\Market;

class Autoloader
{
    private static $PARENT_NAMESPACE = '/Ylkwb/Employees';

    public static function register()
    {
        spl_autoload_register(function ($class) {
            $file = str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
            
            $path = __DIR__ . '/' . $file;
            
            $path = str_replace(self::$PARENT_NAMESPACE, '', $path);
            
            if (file_exists($path)) {
                require $path;
                return true;
            }
            return false;
        });
    }
}