<?php

//https://sohabr.net/habr/post/277095/

namespace Ylkwb\Employees\Core;

final class ManagerDb 
{
    private $dbh;
    
    private $table;
    private $where;
    private $column;
    private $limit;
    private $order;
    private $insertKey;
    private $insertValue;
    private $attr;
    private $isPrint;
    
    private $join = [];
    private $data = [];
    
    private $query;
    private $set;
    
    public function __construct()
    {
        $this->dbh = new DB();
    }
        
    /**
     * Запрос SQL select
     * 
     * @return array
     */
    public function select() : array
    {
        if (is_null($this->column))
            $this->column = '*';
        
        $sql = "SELECT $this->column FROM `$this->table`";
        
        if (is_array($this->join) && count($this->join) > 0)
        {
            $sql .= " " . implode(' ', $this->join);
        }
        
        if (!is_null($this->where))
        {
            $sql .= " WHERE $this->where";
        }
        
        if (!is_null($this->order))
        {
            $sql .= " ORDER BY $this->order";
        }
        
        if (!is_null($this->limit))
        {
            $sql .= " LIMIT $this->limit";
        }
        
        if ($this->isPrint)
        {
            echo '<pre>';
            var_dump($sql, $this->data, $this->attr);
            echo '</pre>';
        }
        
        $result = $this->dbh->fetchAll($sql, $this->data, $this->attr);
        
        $this->exit();
                
        return $result;
    }
    
    /**
     * Запрос SQL insert
     * 
     * @return int
     */
    public function insert() : int
    {
        $sql = "INSERT INTO `$this->table` "
                . "( $this->insertKey ) "
                . "VALUES "
                . "( $this->insertValue )";
        
        if ($this->isPrint)
        {
            echo '<pre>';
            var_dump($sql, $this->data, $this->attr);
            echo '</pre>';
        }
         
        $result = $this->dbh->lastInsertId($sql, $this->data);
        
        $this->exit();
                
        return $result; 
    }
    
    /**
     * Запрос SQL update
     * 
     * @return bool
     */
    public function update() : bool
    {
        $sql = "UPDATE `$this->table` "
                . "SET $this->set";
        
        if (!is_null($this->where))
        {
            $sql .= " WHERE $this->where";
        }
        
        if ($this->isPrint)
        {
            echo '<pre>';
            var_dump($sql, $this->data, $this->attr);
            echo '</pre>';
        }
         
        $result = $this->dbh->execute($sql, $this->data);
        
        $this->exit();
                
        return $result; 
    }
    
    /**
     * Запрос SQL delete
     * 
     * @return bool
     */
    public function delete() : bool
    {
        $sql = "DELETE FROM `$this->table`";
            
        if (!is_null($this->where))
        {
            $sql .= " WHERE $this->where";
        }
        
        if ($this->isPrint)
        {
            echo '<pre>';
            var_dump($sql, $this->data, $this->attr);
            echo '</pre>';
        }
        
        $result = $this->dbh->execute($sql, $this->data);
        
        $this->exit();
                
        return $result; 
    }
    
    /**
     * Указывает печатать запрос или нет
     * 
     * @param type $isPrint
     * @return ManagerDb
     */
    public function isPrint($isPrint = true) : ManagerDb
    {
        $this->isPrint = $isPrint;
        return $this;
    }
       
    /**
     * Устанавливает количество элементов в выборке
     * 
     * @param int $start
     * @param int $count
     * @return ManagerDb
     */
    public function limit (int $start = 0, int $count = 50) : ManagerDb
    {
        $this->limit = '  '.$start.','.$count;
        return $this;
    }
    
    /**
     * Добавляет поле выборки 
     * 
     * @param type $column
     * @return ManagerDb
     */
    public function column($column) : ManagerDb
    {
        if (is_string($this->column) && strlen($this->column) > 0)
        {
            $this->column .=  ", ";
        }
        else
        {
            $this->column = '';
        }
        
        $this->column .= "`$this->table`.`$column` as `$column`";
        
        return $this;
    }
    
    /**
     * Добавляет поля выборки
     * 
     * @param type $columns
     * @return ManagerDb
     */
    public function columns($columns) : ManagerDb
    {
        if (is_array($columns) && count($columns) > 0)
        {
            foreach ($columns as $column)
            {
                $this->column($column);
            }
        }
        
        return $this;
    }
    
    /**
     * Добавляет поле выбборки из другой таблицы 
     * 
     * @param type $table
     * @param type $column
     * @param type $as
     * @return ManagerDb
     */
    public function columnJoin($table, $column, $as = null) : ManagerDb
    {
        if (is_null($as))
        {
            $as = $column;
        }
        
        if (is_string($this->column) && strlen($this->column) > 0)
        {
            $this->column .=  ", ";
        }
        else 
        {
            $this->column =  "";
        }
        
        $this->column .= "`$table`.`$column` as `$as`";
    
        return $this;
    }
    
    /**
     * Добавляет поля выбборки из другой таблицы 
     * 
     * @param type $columns
     * @return ManagerDb
     */
    public function columnsJoin($columns) : ManagerDb
    {
        if (is_array($columns) && count($columns) > 0)
        {
            foreach ($columns as $columnJoin)
            {
                $this->columnJoin($columnJoin[0], $columnJoin[1], $columnJoin[2]);
            }
        }
        
        return $this;
    }

    /**
     * Устанавливает название таблицы для запросов
     * 
     * @param type $key
     * @return ManagerDb
     */
    public function table($key) : ManagerDb
    {
        $this->table  = $key;
        
        return $this;
    }
    
    /**
     * Добавляет сотировку в выборку
     * 
     * @param type $key
     * @param type $ord
     * @return ManagerDb
     */
    public function order($key, $ord = "ASC") : ManagerDb
    {
        if (!is_null($this->order))
        {
            $this->order .= ", ";
        } 
        else 
        {
            $this->order = "";
        }
        
        $this->order .= "`$this->table`.`$key` $ord";
            
        return $this;
    }
    
    /**
     * Добавляет взаимоотношение с другой таблицей в выборке
     * 
     * @param type $table
     * @param type $key
     * @param type $value
     * @param type $compare
     * @return ManagerDb
     */
    public function leftJoin($table, $key, $value = 'ID', $compare = '=') : ManagerDb
    {
        if (!is_array($this->join))
        {
            $this->join = [];
        }
        
        $this->join[] = "LEFT JOIN `$table` ON `$this->table`.`$key` $compare `$table`.`$value`";
        
        return $this;
    }
    
    /**
     * Добавляет условие выборки: "В"
     * 
     * @param type $key
     * @param type $value
     * @return ManagerDb
     */
    public function whereIn($key, $value) : ManagerDb
    {
        $operator = 'AND';
        
        $prepareIn = implode(', ', array_map(function (){
            return '?';
        }, $value));
                
        if (!is_null($this->where) && is_string($this->where)
                && strlen($this->where) > 0)
        {
            $this->where .= " $operator ";
        }
        
        $this->where = "`$this->table`.`$key` IN ($prepareIn)";
        
        $this->data = array_merge($this->data, $value); 
        
        $this->attr = false;
        
        return $this;
    }
    
    /**
     * Добавляет условие выборки 
     * 
     * @param type $key
     * @param type $value
     * @param type $compare
     * @return ManagerDb
     */
    public function where($key, $value, $compare = '=') : ManagerDb
    {
        $operator = 'AND';
        
        if (!is_null($this->where) && is_string($this->where)
                && strlen($this->where) > 0)
        {
            $this->where .= " $operator ";
        }
        
        $this->where = "`$this->table`.`$key` $compare :$key";
        
        $this->data[$key] = $value; 
        
        return $this;
    }
    
    /**
     * Добавляет условие поиска
     * 
     * @param type $table
     * @param type $key
     * @param type $value
     * @return ManagerDb
     */
    public function search($table, $key, $value) : ManagerDb
    {
        $operator = 'AND';
        
        if (!is_null($this->where) && is_string($this->where)
                && strlen($this->where) > 0)
        {
            $this->where .= " $operator ";
        }
        
        $this->where .= "`$this->table`.`ID` IN ("
                . "SELECT `table_search_index`.`$key` AS `ID` " 
                . "FROM `$table` `table_search_index` "
                . "WHERE MATCH (`table_search_index`.`SEARCH_CONTENT`) "
                . "AGAINST (:SEARCH_CONTENT IN BOOLEAN MODE)"
                . ")";
        
        $this->data['SEARCH_CONTENT'] = $value; 
        
        return $this;
    }
    
    /**
     * Устанавливает значение на обновления
     * 
     * @param type $values
     * @return ManagerDb
     */
    public function set($values) : ManagerDb
    {
        foreach ($values as $key=>$value)
        {
            if (is_string($this->set) && strlen($this->set) > 0)
            {
                $this->set .= ", ";
            }
            else
            {
                $this->set = '';
            }
            
            $this->set .= "`$key`=:$key";
            
            $this->data[$key] = $value;
        }    
        
        return $this;
    }
    
    /**
     * Устанавливает добавление первичного ключа в запрос insert
     * 
     * @param type $key
     * @return $this
     */
    public function insertPrimaryKey($key)
    {
        if (is_string($this->insertKey) && strlen($this->insertKey) > 0)
        {
            $insertKey = ", $this->insertKey";
        }
        else
        {
            $insertKey = "";
        }
        
        if (is_string($this->insertValue) && strlen($this->insertValue) > 0)
        {
            $insertValue = ", $this->insertValue";
        }
        else
        {
            $insertValue = "";
        }

        $this->insertKey = "`$key`$insertKey"; 
        $this->insertValue = "NULL$insertValue";

        return $this;
    }
    
    /**
     * Добавляет ключи в запрос на добавление
     * 
     * @param type $keys
     * @return $this
     */
    public function insertKeys($keys)
    {
        foreach ($keys as $key)
        {
            if (is_string($this->insertKey) && strlen($this->insertKey) > 0)
            {
                $this->insertKey .= ", ";
            }
            else
            {
                $this->insertKey = '';
            }
            
            $this->insertKey .= "`$key`";
        }    
        
        return $this;
    }
    
    /**
     * Добавляет значения в запрос на добавление
     * 
     * @param type $values
     * @return $this
     */
    public function insertValue($values)
    {
        foreach ($values as $key=>$value)
        {
            if (is_string($this->insertValue) && strlen($this->insertValue) > 0)
            {
                $this->insertValue .= ", ";
            }
            else
            {
                $this->insertValue = '';
            }
            
            $this->insertValue .= ":$key";
            $this->data[$key] = $value;
        }    
        return $this;
    }
    
    /**
     * Удаляет все заначение созданные ранее
     * 
     * @return ManagerDb
     */
    private function exit() : ManagerDb
    {
        $this->table = null;
        $this->where = null;
        $this->column = null;
        $this->limit = null;
        $this->order = null;
        $this->query = null;
        $this->set = null;
        $this->attr = null;
        $this->insertKey = null;
        $this->insertValue = null;
        $this->isPrint = null;
        
        $this->data = [];
        $this->join = [];
        
        return $this;
    }
}
