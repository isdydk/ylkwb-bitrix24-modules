<?php

namespace Ylkwb\Employees\Core;

class DB 
{
    public function __construct()
    {
        $connections = \Bitrix\Main\Config\Configuration::getValue('connections');
        
        $connectionsDefault = $connections["default"];
        
        if (is_array($connectionsDefault) 
                && count($connectionsDefault) > 0)
        {
            try 
            {
                $this->dbh = $this->getMySqlConnection($connectionsDefault);  
            } 
            catch (Exception $exc) 
            {
                //echo $exc->getTraceAsString();
            }
        }
        
    }
    
    /**
     * Отправить запрос
     * 
     * @param string $sql - строка запроса
     * @param array $data - данные запроса
     * @param type $isAttr - устанавливать атрибуты запроса
     * @return bool
     */
    public function execute(string $sql, array $data = [], $isAttr = true) : bool
    {
        if (!is_null($this->dbh))
        {
            $this->setAttribute($isAttr);
            
            try 
            {
                $sth = $this->dbh->prepare($sql);
                
                return $sth->execute($data);
            }
            catch (Exception $exc)
            {
                //Todo logger
                var_dump($exc);
            }

        }
        
        return [];
    }
    
    /**
     * Отправить запрос и получить все строки
     * 
     * @param string $sql - строка запроса
     * @param array $data - данные запроса
     * @param type $isAttr - устанавливать атрибуты запроса
     * @return bool
     */
    public function fetchAll(string $sql, array $data = [], $isAttr = true) : array
    {
        if (!is_null($this->dbh))
        {
            $this->setAttribute($isAttr);
                  
            try 
            {
                $sth = $this->dbh->prepare($sql);
                
                $isExecute = $sth->execute($data);

                if ($isExecute)
                {
                    return $sth->fetchAll(\PDO::FETCH_ASSOC);
                }
            }
            catch (Exception $exc)
            {
                //Todo logger
                var_dump($exc);
            }

        }
        
        return [];
    }
        
    /**
     * Отправить запрос и получить строку
     * 
     * @param string $sql - строка запроса
     * @param array $data - данные запроса
     * @param type $isAttr - устанавливать атрибуты запроса
     * @return bool
     */
    public function fetch(string $sql, array $data = [], $isAttr = true) : array
    {
        if (!is_null($this->dbh))
        {
            $this->setAttribute($isAttr);
            
            try 
            {
                $sth = $this->dbh->prepare($sql);

                $isExecute = $sth->execute($data);

                if ($isExecute)
                {
                    return $sth->fetch(\PDO::FETCH_ASSOC);
                }
            }
            catch (Exception $exc)
            {
                //Todo logger
                var_dump($exc);
            }

        }
        
        return [];
    }
        
    /**
     * Отправить запрос и получить id последнего добавленного элемента
     * 
     * @param string $sql - строка запроса
     * @param array $data - данные запроса
     * @param type $isAttr - устанавливать атрибуты запроса
     * @return bool
     */
    public function lastInsertId(string $sql, array $data = [], $isAttr = true) : int
    {
        if (!is_null($this->dbh))
        {
            $this->setAttribute($isAttr);
            
            try 
            {
                $sth = $this->dbh->prepare($sql);
                
                $isExecute = $sth->execute($data);
                
                if ($isExecute)
                {
                    return intval($this->dbh->lastInsertId());
                }
                
            }
            catch (Exception $exc)
            {
                //Todo logger
                var_dump($exc);
            }

        }
        
        return 0;
    }
        
    /**
     * Установить атрибуты запроса
     * 
     * @param type $isAttr
     * @return int
     */
    public function setAttribute($isAttr = true) : int
    {
        if (!is_null($this->dbh) && $isAttr)
        {
            $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        
        return 0;
    }

    /**
     * Получить коннектор PDO
     * 
     * @param type $dbmsName - имя СУБД
     * @param type $connection - параметры БД
     * @return \PDO
     */
    public function getConnection($dbmsName, $connection) 
    {
        return new \PDO(
                $dbmsName. ':'
                . 'dbname=' . $connection['database'] . ';'
                . 'host=' . $connection['host'],
                $connection['login'],
                $connection['password']
        );
    }
    
    /**
     * Получить коннектор Mysql PDO
     * 
     * @param type $connection - параметры БД
     * @return \PDO
     */
    public function getMySqlConnection($connection) 
    {
        return $this->getConnection('mysql', $connection);
    }
}
