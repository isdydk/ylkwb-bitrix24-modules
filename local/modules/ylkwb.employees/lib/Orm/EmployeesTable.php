<?php

namespace Ylkwb\Employees\Orm;

use  Bitrix\Main\Entity\DataManager;

class EmployeesTable extends DataManager
{
    public static function getTableName()
    {
        return 'ylkwb_employees_list';
    } 
    
    public static function getMap()
    {
        return array(
            new \Bitrix\Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new \Bitrix\Main\Entity\StringField('FIO', array(
                'required' => true,
            )),
            new \Bitrix\Main\Entity\StringField('PHONE', array(
                'required' => true,
            )),
            new \Bitrix\Main\Entity\StringField('POST', array(
                'required' => true,
            )),
            new \Bitrix\Main\Entity\StringField('OFFICE', array(
                'required' => true,
            )),
            new \Bitrix\Main\Entity\StringField('ADDRESS', array(
                'required' => true,
            )),
        );
    }
}
