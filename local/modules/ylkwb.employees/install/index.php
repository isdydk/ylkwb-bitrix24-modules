<?

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('ylkwb_employees'))
{
    return;
}

class ylkwb_employees extends CModule 
{
    var $MODULE_ID;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = 'Y';
    var $errors = '';

    public function __construct() 
    {
        $arModuleVersion = array();
        
        include(__DIR__ . "/version.php");

        $this->MODULE_ID = 'ylkwb.employees';

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("YLKWB_EMPLOYEES_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("YLKWB_EMPLOYEES_MODULE_DESC");

        $this->PARTNER_NAME = Loc::getMessage("YLKWB_EMPLOYEES_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("YLKWB_EMPLOYEES_PARTNER_URI");

        $this->MODULE_SORT = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
        $this->MODULE_GROUP_RIGHTS = "Y";
    }

    public function GetPath() 
    {
        return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    public function DoInstall() 
    {
        global $APPLICATION;

        if ($this->isVersionD7())
        {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
            
            $this->InstallDB();
        }
        else 
        {
            $APPLICATION->ThrowException(Loc::getMessage("YLKWB_EMPLOYEES_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("YLKWB_EMPLOYEES_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallDB();
        
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(Loc::getMessage("YLKWB_EMPLOYEES_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep.php");
    }
    
    public function InstallDB()
    {
        global $DB, $APPLICATION;

        $this->errors = false;

        if (!$DB->Query("SELECT 'x' FROM ylkwb_employees_list", true))
        {
            $this->errors = $DB->RunSQLBatch($this->GetPath() . '/install/db/' . mb_strtolower($DB->type) . '/install.sql');
        }

        //region INDEXES
        if ($DB->type === "MYSQL")
        {
            if ($DB->Query("CREATE FULLTEXT INDEX IX_YLKWB_EMPLOYESS_LIST_SEARCH ON ylkwb_employees_list(SEARCH_CONTENT)", true))
            {
                \Bitrix\Main\Config\Option::set("main", "~ft_ylkwb_employees_list", serialize(["SEARCH_CONTENT" => true]));
            }
        }
        //endregion

        if (is_array($this->errors))
        {
            $GLOBALS['errors'] = $this->errors;
            $APPLICATION->ThrowException(implode(' ', $this->errors));

            return false;
        }
        
        $DB->RunSQLBatch($this->GetPath() . '/install/db/' . mb_strtolower($DB->type) . '/data.sql');

        $this->installAgents();
        $this->installHandler();
        
        return true;
    }

    public function UnInstallDB()
    {
            global $DB, $APPLICATION, $CACHE_MANAGER, $stackCacheManager, $USER_FIELD_MANAGER;
            
            $this->uninstallAgents();
            $this->uninstallHandler();
            
            $this->errors = false;
            
            $this->errors = $DB->RunSQLBatch($this->GetPath() . '/install/db/' . mb_strtolower($DB->type) . '/uninstall.sql');

            if (is_array($this->errors))
            {
                $APPLICATION->ThrowException(implode('<br />', $this->errors));

                return false;
            }

            return true;
    }
    
    private function installAgents()
    {
        $startTime = ConvertTimeStamp(time() + \CTimeZone::GetOffset() + 60, 'FULL');

        CAgent::AddAgent(
            '\Ylkwb\Employees\Agent\Search\EmployeesSearchContentRebuildAgent::run();', 
            $this->MODULE_ID,
            'Y', 
            2, 
            '', 
            'Y', 
            $startTime, 
            100, 
            false, 
            false
        );
    }
    
    private function installHandler()
    {
        $eventM = \Bitrix\Main\EventManager::getInstance();
                
        $eventM->registerEventHandler(
            "main",
            "OnUserLoginExternal",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnUserLoginExternal"
        );
        
        $eventM->registerEventHandler(
            "main",
            "OnExternalAuthList",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnExternalAuthList"
        );
        
        $eventM->registerEventHandler(
            "main",
            "onBeforeUserLoginByHttpAuth",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "onBeforeUserLoginByHttpAuth"
        );
        
        $eventM->registerEventHandler(
            "main",
            "OnEpilog",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnEpilog"
        );
        
    }
    
    private function uninstallAgents()
    {
        $eventM = \Bitrix\Main\EventManager::getInstance();
        
        $eventM->unRegisterEventHandler(
            "main",
            "OnUserLoginExternal",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnUserLoginExternal"
        );
        
        $eventM->unRegisterEventHandler(
            "main",
            "OnExternalAuthList",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnExternalAuthList"
        );
        
        $eventM->unRegisterEventHandler(
            "main",
            "onBeforeUserLoginByHttpAuth",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "onBeforeUserLoginByHttpAuth"
        );
        
        $eventM->unRegisterEventHandler(
            "main",
            "OnEpilog",
            $this->MODULE_ID,
            "__YLWB2Auth",
            "OnEpilog"
        );
    }
    
    private function uninstallHandler()
    {
        CAgent::RemoveModuleAgents($this->MODULE_ID);
    }
}

?>