INSERT INTO `ylkwb_employees_post`
(
    `ID`, `NAME`
) 
VALUES (
    NULL,
    'Администратор'
),
(
    NULL,
    'Оператор'
),
(
    NULL,
    'Управляющий'
),
(
    NULL,
    'Специалист'
),
(
    NULL,
    'Кассир'
),
(
    NULL,
    'Бухгалтер'
);

INSERT INTO `ylkwb_employees_office`
(
    `ID`, `NAME`, `ADDRESS`
) 
VALUES (
    NULL,
    'Северный',
    'Дзержинского, 20'
),
(
    NULL,
    'Центральный',
    'Ленина, 51'
),
(
    NULL,
    'Западный',
    'Солнечная, 37А'
),
(
    NULL,
    'Октябрьский',
    'Октябрьский, 98'
);

INSERT INTO `ylkwb_employees_list` 
(
    `ID`, `FIO`, `PHONE`, `POST_ID`, `OFFICE_ID`
) 
VALUES 
(
    NULL, 
    'Иванов Дмитрий Сергеевич', 
    '2137', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Оператор'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Северный'
    )
)
,(   
    NULL, 
    'Кочкина Дарья Алексеевна', 
    '2101', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Администратор'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Центральный'
    )
)
,(
    NULL, 
    'Сергеев Юрий Витальевич', 
    '2112', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Управляющий'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Центральный'
    )
)
,(
    NULL, 
    'Баранов Сергей Вадимович',
    '2117', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Специалист'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Северный'
    )
)
,(
    NULL, 
    'Михайлов Максим Юрьевич', 
    '2122', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Оператор'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Западный'
    )
)
,(
    NULL, 
    'Фомина Анна Валентиновна', 
    '2120', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Кассир'
    ),  
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Центральный'
    )
)
,(
    NULL, 
    'Носков Андрей Витальевич', 
    '2131', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Специалист'
    ),  
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Октябрьский'
    )
)
,(
    NULL, 
    'Соколова Анна Валерьевна', 
    '2130', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Бухгалтер'
    ), 
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Центральный'
    )
)
,(
    NULL, 
    'Шахматов Алексей Алексеевич', 
    '2125', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Специалист'
    ),  
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Западный'
    )
)
,(
    NULL, 
    'Ершов Дмитрий Иванович', 
    '2128', 
    (
        SELECT `ylkwb_employees_post`.`ID` 
        FROM `ylkwb_employees_post` 
        WHERE `ylkwb_employees_post`.`NAME` = 'Специалист'
    ),  
    (
        SELECT `ylkwb_employees_office`.`ID` 
        FROM `ylkwb_employees_office` 
        WHERE `ylkwb_employees_office`.`NAME` = 'Октябрьский'
    )  
);

INSERT INTO `ylkwb_employees_list_index`
(
    `ID`, `SEARCH_CONTENT`
) 
VALUES 
( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Иванов Дмитрий Сергеевич'
    ), 
    'Иванов Дмитрий Сергеевич 2137 Оператор Северный Дзержинского, 20'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Кочкина Дарья Алексеевна'
    ), 
    'Кочкина Дарья Алексеевна 2101 Администратор Центральный Ленина, 51'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Сергеев Юрий Витальевич'
    ), 
    'Сергеев Юрий Витальевич 2112 Управляющий Центральный Ленина, 51'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Баранов Сергей Вадимович'
    ), 
    'Баранов Сергей Вадимович 2117 Специалист Северный Дзержинского, 20'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Михайлов Максим Юрьевич'
    ), 
    'Михайлов Максим Юрьевич 2122 Оператор Западный Солнечная, 37А'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Фомина Анна Валентиновна'
    ), 
    'Фомина Анна Валентиновна 2120 Кассир Центральный Ленина, 51'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Носков Андрей Витальевич'
    ), 
    'Носков Андрей Витальевич 2131 Специалист Октябрьский Октябрьский, 98'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Соколова Анна Валерьевна'
    ), 
    'Соколова Анна Валерьевна 2130 Бухгалтер Центральный Ленина, 51'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Шахматов Алексей Алексеевич'
    ), 
    'Шахматов Алексей Алексеевич 2125 Специалист Западный Солнечная, 37А'
)
,( 
    (
        SELECT `ylkwb_employees_list`.`ID` 
        FROM `ylkwb_employees_list` 
        WHERE `ylkwb_employees_list`.`FIO` = 'Ершов Дмитрий Иванович'
    ), 
    'Ершов Дмитрий Иванович 2128 Специалист Октябрьский Октябрьский, 98'
);


INSERT INTO `ylkwb_employees_users`
(
    `ID`, `LOGIN`, `PASSWORD`, `LAST_AUTH`
) 
VALUES 
( 
    NULL,
    'admin',
    '$2y$10$A5KGFvDS7UbwVudl.eGtd.ndkwp3mJA6mj3LueZMabgBd9rSQlVjG',
    NULL
)
,( 
    NULL,
    'manager',
    '$2y$10$FE3tL3Bb0yNdrbTe1KNf7OVxcRXogqS9XTLBDmRoHw4hpmE1Rz5QG',
    NULL
);
