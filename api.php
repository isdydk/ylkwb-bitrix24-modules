<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Ylkwb\Employees\Table\EmployeesList;
use Ylkwb\Employees\Table\EmployeesOffice;
use Ylkwb\Employees\Table\EmployeesPost;

global $USER;

$context = \Bitrix\Main\Context::getCurrent();

$request = $context->getRequest(); 

$method = $request->get("method");

$status_answer = 'error';
$status_message = '';

if ($USER->IsAuthorized())
{
    if (\Bitrix\Main\Loader::includeModule('ylkwb.employees'))
    {
        $valid = new Ylkwb\Employees\Validate();

        switch ($method)
        {
            /**
             * Поиск сотрудника
             * 
             * Параметры:
             *  text - текст
             */
            case 'search': 
                
                $text = $request->get("text");
                
                if (!empty($text))
                {
                    $el = new EmployeesList();
                    $out = $el->search($text);
                    $status_answer = 'ok';
                }
                else
                {
                    $status_message = 'Не указан обязательный параметр!';
                }
            break;
            
            /**
             * Добавить сотрудника
             * 
             * Параметры:
             *  fio - ФИО
             *  phone - Телефон
             *  post - Название должность
             *  office - Название офиса
             */
            case 'add': 
                $fio = $request->get("fio");
                $phone = $valid->getFormatedPhone($request->get("phone"));
                $post = $request->get("post");
                $office = $request->get("office");
                
                if ($valid->isValidString($office))
                {
                    $eof = new EmployeesOffice();

                    $resOf = $eof->getList([], [
                        'NAME' => $office
                    ], ['ID']);

                    $officeData = array_shift($resOf);

                    if (!isset($officeData['ID']) || intval($officeData['ID']) <= 0)
                    {
                        $officeId = 2;
                    }
                    else
                    {
                        $officeId = intval($officeData['ID']);
                    }
                }
                else
                {
                    $officeId = 2;
                }
                
                if ($valid->isValidString($post))
                {
                    $ep = new EmployeesPost();

                    $resP = $ep->getList([], [
                        'NAME' => $post
                    ], ['ID']);

                    $postData = array_shift($resP);

                    if (!isset($postData['ID']) || intval($postData['ID']) <= 0)
                    {
                        $postId = 2;
                    }
                    else
                    {
                        $postId = intval($postData['ID']);
                    }
                }
                else
                {
                    $postId = 2;
                }
                $el = new EmployeesList();

                $fields = $el->getFields($fio, $phone, $postId, $officeId);
                
                if ($valid->isValidArray($fields) && count($fields) === 4)
                {
                    $out = $el->add($fields);

                    $status_answer = 'ok';
                }
                else
                {
                    $status_message = 'Не указан обязательный параметр!';
                }
            break;
            
            /**
             * Обновить сотрудника
             * 
             * Параметры:
             *  id - Идентификатор сотрудника
             *  fio - ФИО
             *  phone - Телефон
             *  post - Название должность
             *  office - Название офиса
             */
            case 'update': 
                $id = $valid->getInt($request->get("id"));
                $fio = $request->get("fio");
                $phone = $valid->getFormatedPhone($request->get("phone"));
                $post = $request->get("post");
                $office = $request->get("office");
                
                if (is_string($office) && strlen($office) > 0)
                {
                    $eof = new EmployeesOffice();

                    $resOf = $eof->getList([], [
                        'NAME' => $office
                    ], ['ID']);

                    $officeData = array_shift($resOf);

                    if (!isset($officeData['ID']) || intval($officeData['ID']) <= 0)
                    {
                        $officeId = 0;
                    }
                    else
                    {
                        $officeId = intval($officeData['ID']);
                    }
                }
                else
                {
                    $officeId = 0;
                }
                
                if (is_string($post) && strlen($post) > 0)
                {
                    $ep = new EmployeesPost();

                    $resP = $ep->getList([], [
                        'NAME' => $post
                    ], ['ID']);

                    $postData = array_shift($resP);
                    
                    if (!isset($postData['ID']) || intval($postData['ID']) <= 0)
                    {
                        $postId = 0;
                    }
                    else
                    {
                        $postId = intval($postData['ID']);
                    }
                }
                else
                {
                    $postId = 0;
                }

                $el = new EmployeesList();

                $fields = $el->getFields($fio, $phone, $postId, $officeId);
                
                if ($valid->isValidInt($id)
                        && $valid->isValidArray($fields))
                {
                    $out = $el->update($id, $fields);

                    $status_answer = 'ok';
                }
                else
                {
                    $status_message = 'Не указан обязательный параметр!';
                }
                
            break;
            
            /**
             * Удалить сотрудника
             * 
             * Параметры:
             *  id - Идентификатор сотрудника
             */
            case 'delete': 
                $id = $valid->getInt($request->get("id"));
                
                if ($valid->isValidInt($id))
                {
                    $el = new EmployeesList();

                    $out = $el->delete($id);

                    $status_answer = 'ok';
                }
                else
                {
                    $status_message = 'Не указан обязательный параметр!';
                }
            break;
            
            /**
             * Получить сотрудника
             * 
             * Параметры:
             *  id - Идентификатор сотрудника
             */
            case 'get': 
                $id = $valid->getInt($request->get("id"));
                
                if (is_int($id) && intval($id) > 0)
                {
                    $el = new EmployeesList();

                    $out = $el->get($id);

                    $status_answer = 'ok';
                }
                else
                {
                    $status_message = 'Не указан обязательный параметр!';
                }
            break;
            
            /**
             * Получить сотрудников
             * 
             * Параметры:
             *  order - сортировка
             *  filter - фильтр
             *  select - выборка
             */
            case 'list': 
                $order = $request->get("order");
                $filter = $request->get("filter");
                $select = $request->get("select");
                
                $el = new EmployeesList();
                    
                $out = $el->getList($order, $filter, $select);
                
                $status_answer = 'ok';
                
            break;
        
            /**
             * Получить офисы
             * 
             * Параметры:
             *  order - сортировка
             *  filter - фильтр
             *  select - выборка
             */
            case 'offices': 
                $order = $request->get("order");
                $filter = $request->get("filter");
                $select = $request->get("select");
                
                $eof = new EmployeesOffice();
                
                $out = $eof->getList($order, $filter, $select);
                
                $status_answer = 'ok';
            break;
        
            /**
             * Получить должности
             * 
             * Параметры:
             *  order - сортировка
             *  filter - фильтр
             *  select - выборка
             */
            case 'post': 
                $order = $request->get("order");
                $filter = $request->get("filter");
                $select = $request->get("select");
                
                $ep = new EmployeesPost();
                    
                $out = $ep->getList($order, $filter, $select);
                
                $status_answer = 'ok';
            break;
            
            default :
                $status_message = 'Не указан метод!';
        }
    }
    else
    {
        $status_message = 'Ошибка модуля!';
    }
}
else
{
    $status_message = 'Ошибка авторизации!';
}

echo json_encode(array(
    'status_answer' => $status_answer,
    'status_message' => $status_message,
    'out' => $out,
));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>

